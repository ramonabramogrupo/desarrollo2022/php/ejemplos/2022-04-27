<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $circulos=[
            20,30,40
        ];
        
        $cuadrados=[
          20,30  
        ];
        
        
        // imprimir los circulos
        
        foreach ($circulos as $radio){
            // comienzo de impresion de circulos
        ?>
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
        width="140" height="140" viewBox="-10 -10 140 140"
        style="background-color: white">
          <circle cx="60" cy="60" r="<?= $radio?>" fill="black" />
        </svg>
        <?php  
            // final de impresion de circulos
        }
        
        
        // imprimir los cuadrados
        
        foreach ($cuadrados as $lado){
            // comienzo impresion cuadrados
        ?>
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
        width="140" height="140" viewBox="-10 -10 140 140"
        style="background-color: white">
        <rect x="10" y="10" width="<?= $lado ?>" height="<?= $lado ?>"/>
        </svg>
        <?php
            // final impresion cuadrados
        }
        ?>
       
                
        
        
        
        
    </body>
</html>
