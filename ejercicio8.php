<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <form method="get">
            <div>
                <label for="numero1">Numero 1</label>
                <input type="number" id="numero1" name="numeros[]" class="form-control">
            </div>
            
            <div>
                <label for="numero2">Numero 2</label>
                <input type="number" id="numero2" name="numeros[]" class="form-control">
            </div>
            
            <button class="btn btn-primary">Enviar</button>
        </form>

        <?php
        if ($_GET) {
            //principio comprobacion de si llegan datos
            
            // ahora leo los dos numeros como array
            $numeros = $_GET["numeros"];
            
            // creo un array asociativo para almacenar los resultados
            $resultados=[
                "suma" => $numeros[0]+$numeros[1],
                "resta" => $numeros[0]-$numeros[1],
                "producto" => $numeros[0]*$numeros[1],
                "cociente" => $numeros[0]/$numeros[1],                                
                "resto" => $numeros[0]%$numeros[1],                
                "potencia" => $numeros[0]**$numeros[1],
            ];

            ?>
            <table>
            <?php
            
            foreach($resultados as $indice => $valor){
                // principio del lazo para mostrar los resultados
            ?>
                <tr>
                    <td><?= $indice ?></td>
                    <td><?= $valor ?></td>
                </tr>
            <?php
            
                // final del lazo para mostrar los resultados
            }
            ?>
                
            </table>
            <?php
            // final de comprobacion de si llegan datos
        }
        ?>
    </body>
</html>
